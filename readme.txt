=== GAS WordPress ===
Contributors: ArlingtonCounty, dbarlett
Tags: analytics, gas
Requires at least: 3.5
Tested up to: 4.9.6
Stable tag: 1.2.9
License: GPL-3.0
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Add the advanced tracking of Google Analytics on Steroids (downloads, YouTube, Vimeo, and more) to your site.

== Description ==

[Google Analytics on Steroids](https://github.com/CardinalPath/gas) (GAS) is a
powerful wrapper library for Google Analytics that enables enhanced tracking, including:

* Outbound links
* Downloads
* Forms
* mailto:
* YouTube, Vimeo, and HTML5 video events

This plugin makes it easy to add GAS to your WordPress site. All logged-out visitors and logged-in users without
the [edit_posts](https://codex.wordpress.org/Roles_and_Capabilities#edit_posts)
capability (i.e. [Subscriber](https://codex.wordpress.org/Roles_and_Capabilities#Subscriber)
) are tracked.

Written by the [Arlington County](http://www.arlingtonva.us) Department of Technology Services. Thanks to:

* [Cardinal Path](http://www.cardinalpath.com/) for [Google Analytics on Steroids](https://github.com/CardinalPath/gas)
* [Gilbert Pellegrom](http://gilbert.pellegrom.me/) for the [WordPress Settings Framework](https://github.com/gilbitron/WordPress-Settings-Framework)
* [Phillihp Harmon](http://phillihp.com/) for code contributions

== Installation ==

1. Upload `gas-wordpress` to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress
1. Go to Settings > Google Analytics on Steroids and fill in your GA property ID(s) and domains(s). All other fields are optional.

== Changelog ==

= 1.2.9 =
* Fix: syntax error, PHP 7.2 deprecation warning
* Change: SemVer
* Change: Bumped WordPress compatibility to 4.9.6

= 0.28 =
* Update Arlington URLs
* Bump compatibility to WP 4.6

= 0.27 =
* Enqueue jQuery in oEmbeds

= 0.26 =
* Output tracking code inside oEmbeds
* Bump compatibility to WP 4.4

= 0.25 =
* Add setting to track printing

= 0.24 =
* Formatting per WP Code Standards
* Output escaping
* Bump compatibility to WP 4.3

= 0.23 =
* Formatting per WP Code Standards
* Output escaping
* Bump compatibility to WP 4.2.3

= 0.22 =
* Bump compatibility to WP 4.2-beta2

= 0.21 =
* Spacing per WP code standards
* Lower wp_head priority
* Bump compatibility to WP 4.1.1

= 0.20 =
* Only insert tracking code if at least one ID and domain are defined
* Bump compatibility to WP 4.1

= 0.19 =
* Bump compatibility to WP 4.0

= 0.18 =
* Bump compatibility to WP 3.9.2

= 0.17 =
* Bump compatibility to WP 3.9.1

= 0.16 =
* Bump compatibility to WP 3.9

= 0.15 =
* Bump compatibility to WP 3.8.1

= 0.14 =
* Update to GAS 1.11.0
* Bump compatibility to WP 3.7.1

= 0.13 =
* Escape ampersand in filtered YouTube oembed URLs
* Bump compatibility to WP 3.7

= 0.12 =
* Fix PHP Notice
* Bump compatibility to WP 3.6.1

= 0.11 =
* Filter oembed_dataparse to append enablejsapi=1 to YouTube embed iframe src if YouTube tracking enabled
* Fix typo causing errors in IE with certain settings
* Clean up setting descriptions

= 0.10 =
* Add setting to honor [Do Not Track HTTP Header](http://donottrack.us/)

= 0.9 =
* First public beta
