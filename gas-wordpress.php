<?php
/*
Plugin Name: GAS WordPress
Plugin URI: https://bitbucket.org/ArlingtonCounty/gas-wordpress
Description: Add Google Analytics on Steroids tracking
Version: 1.2.9
Author: Arlington County
Author URI: https://www.arlingtonva.us
License: GPL-3.0

Copyright 2016 County Board of Arlington County (email: webmaster@arlingtonva.us)

This file is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class GAS {

	private $settings_page;
	private $plugin_url;
	private $plugin_path;
	private $l10n;
	private $wpsf;

	function __construct() {
		$this->settings_page = plugin_dir_path( __FILE__ ) . 'settings/settings-gas.php';
		$this->plugin_url    = plugin_dir_url( __FILE__ );
		$this->plugin_path   = plugin_dir_path( __FILE__ );
		$this->l10n = 'wp-settings-framework';
		add_action( 'admin_menu', array( &$this, 'admin_menu' ), 99 );

		// Include and create a new WordPressSettingsFramework.
		require_once( $this->plugin_path . 'wp-settings-framework.php' );
		$this->wpsf = new WordPressSettingsFramework( $this->settings_page );
		// Add an optional settings validation filter (recommended).
		add_filter( $this->wpsf->get_option_group() . '_settings_validate', array( &$this, 'validate_settings' ) );

		$settings = wpsf_get_settings( $this->settings_page );

		add_filter( 'oembed_dataparse', array( $this, 'enable_apis' ), 10, 3 );

		add_action( 'wp_head', array( $this, 'inject_gas_snippet' ), 15, 0 );
		add_action( 'embed_head', array( $this, 'enqueue_scripts' ), 15, 0 );
		add_action( 'embed_head', array( $this, 'inject_gas_snippet' ), 20, 0 );
	}

	function admin_menu() {
		add_options_page(
			__( 'Google Analytics on Steroids', $this->l10n ),
			__( 'Google Analytics on Steroids', $this->l10n ),
			'manage_options',
			'gas-plugin',
			array( &$this, 'settings_page' )
		);
	}

	function settings_page() {
		?>
		<div class="wrap">
			<h2><?php esc_html_e( 'Google Analytics on Steroids', $this->l10n ) ?></h2>
			<?php
			$this->wpsf->settings();
			?>
		</div>
		<?php
	}

	function validate_settings( $input ) {
		$ua_pattern     = '/^ua-\d{4,}-\d{1,}$/i';
		$domain_pattern = '/^\.?[-a-z0-9]+\.[a-z]{2,6}$/';

		foreach ( $input as $key => $value ) {
			$input[ $key ] = sanitize_text_field( $value );
		}

		$raw_uas   = explode( ',', $input['settingsgas_ga_property-ids'] );
		$valid_uas = array();
		foreach ( $raw_uas as $value ) {
			if ( preg_match( $ua_pattern, trim( $value ) ) ) {
				$valid_uas[] = trim( $value );
			} else {
				add_settings_error(
					'settingsgas_ga_property-ids',
					'settings_updated',
					__( 'Invalid GA property ID', $this->l10n ),
					'error'
				);
			}
		}
		$input['settingsgas_ga_property_ids'] = implode( ',', $valid_uas );

		$raw_domains   = explode( ',', $input['settingsgas_ga_domains'] );
		$valid_domains = array();
		foreach ( $raw_domains as $value ) {
			if ( preg_match( $domain_pattern, trim( $value ) ) ) {
				$valid_domains[] = trim( $value );
			} else {
				add_settings_error(
					'settingsgas_ga_domains',
					'settings_updated',
					__( 'Invalid domain', $this->l10n ),
					'error'
				);
			}
		}
		$input['settingsgas_ga_domains'] = implode( ',', $valid_domains );

		$raw_percentages   = explode( ',', $input['settingsgas_youtube_percentages'] );
		$valid_percentages = array();
		foreach ( $raw_percentages as $value ) {
			$raw_int = (int) $value;
			if ( ( 0 <= $raw_int ) && ( $raw_int <= 100  ) ) {
				$valid_percentages[] = $raw_int;
			}
		}
		$input['settingsgas_youtube_percentages'] = implode( ',', $valid_percentages );

		return $input;
	}

	/**
	 *
	 * Check if Do Not Track HTTP header is present in request and set to 1 (enabled). Based on code
	 * from http://donottrack.us/application.
	 *
	 * @return bool true if Do Not Track enabled, false if disabled or header not present
	 */
	function dnt_enabled() {
		$dnt_header = 'HTTP_DNT';

		if ( ( array_key_exists( $dnt_header, $_SERVER ) ) and ( '1' === $_SERVER[ $dnt_header ] ) ) {
			return true;
		} else {
			return false;
		}
	}


	/**
	 * Enqueue JS.
	 *
	 * @link https://developer.wordpress.org/reference/hooks/wp_enqueue_scripts/
	 */
	function enqueue_scripts() {
		wp_enqueue_script( 'jquery' );
	}

	function inject_gas_snippet() {
		$settings = wpsf_get_settings( $this->settings_page );

		if ( ( '1' === $settings['settingsgas_dnt_header'] ) and ( $this->dnt_enabled() ) ) {
			$track_visitor = false;
		} elseif ( current_user_can( 'edit_posts' ) ) {
			$track_visitor = false;
		} else {
			$property_ids = explode( ',', $settings['settingsgas_ga_property-ids'] );
			$domains = explode( ',', $settings['settingsgas_ga_domains'] );
			if ( is_array( $property_ids ) && count( $property_ids ) && is_array( $domains ) && count( $domains ) ) {
				$track_visitor = true;
			}
		}

		if ( $track_visitor ) {
			echo "<script type='text/javascript'>var _gas = _gas || [];";

			foreach ( $property_ids as $property_id ) {
				echo "_gas.push(['_setAccount', '" . esc_js( trim( $property_id ) ) . "']);";
			}

			foreach ( $domains as $domain ) {
				echo "_gas.push(['_setDomainName', '" . esc_js( trim( $domain ) ) . "']);";
			}
			if ( 1 <= count( $domains ) ) {
				echo "_gas.push(['_gasMultiDomain', 'click']);";
			}

			if ( '1' === $settings['settingsgas_page_trackpageviews'] ) {
				echo "_gas.push(['_trackPageview']);";
			}

			if ( '1' === $settings['settingsgas_page_trackoutboundlinks'] ) {
				echo "_gas.push(['_gasTrackOutboundLinks']);";
			}

			if ( '1' === $settings['settingsgas_page_trackmailto'] ) {
				echo "_gas.push(['_gasTrackMailto']);";
			}

			if ( '1' === $settings['settingsgas_page_trackforms'] ) {
				echo "_gas.push(['_gasTrackForms']);";
			}

			if ( '1' === $settings['settingsgas_page_trackmaxscroll'] ) {
				echo "_gas.push(['_gasTrackMaxScroll']);";
			}

			if ( '1' === $settings['settingsgas_download_track'] ) {
				$extensions = $settings['settingsgas_download_extensions'];
				echo "_gas.push(['_gasTrackDownloads', {";
				echo "category: '" . esc_js( $settings['settingsgas_download_category'] ). "',";
				echo "extensions: ['" . esc_js( $extensions ). "']";
				echo '}]);';
			}

			if ( '1' === $settings['settingsgas_youtube_track'] ) {
				echo "_gas.push(['_gasTrackYoutube', {";
				echo 'percentages: [' . esc_js( $settings['settingsgas_youtube_percentages'] ) . '],';
				echo "category: '" . esc_js( $settings['settingsgas_youtube_category'] ). "'";
				if ( '0' === $settings['settingsgas_youtube_forcereload'] ) {
					echo ',force: false}]);';
				} else {
					echo '}]);';  // 'force: true' is GAS default
				}
			}

			if ( '1' === $settings['settingsgas_vimeo_track'] ) {
				echo "_gas.push(['_gasTrackVimeo', {";
				echo "category: '" . esc_js( $settings['settingsgas_vimeo_category'] ) . "'";
				if ( '0' === $settings['settingsgas_youtube_forcereload'] ) {
					echo ',force: false}]);';
				} else {
					echo '}]);';  // 'force: true' is GAS default
				}
			}

			if ( '1' === $settings['settingsgas_html5_audio'] ) {
				echo "_gas.push(['_gasTrackAudio']);";
			}

			if ( '1' === $settings['settingsgas_html5_video'] ) {
				echo "_gas.push(['_gasTrackVideo']);";
			}

			echo "(function() {var ga = document.createElement('script');
ga.type = 'text/javascript';ga.async = true;
ga.src = '//cdnjs.cloudflare.com/ajax/libs/gas/1.11.0/gas.min.js';
var s = document.getElementsByTagName('script')[0];
s.parentNode.insertBefore(ga, s);
})();";

			if ( '1' === $settings['settingsgas_page_trackprint'] ) {
				echo "jQuery( document ).ready(function() {
	var afterPrint = function() {
		_gas.push( ['_trackEvent', 'Print', 'Print'] );
	};
	if ( window.matchMedia ) {
		var mediaQueryList = window.matchMedia( 'print' );
		mediaQueryList.addListener( function( mql ) {
			if ( mql.matches ) {
				afterPrint();
			}
		});
	}
	window.onafterprint = afterPrint;
}());";
			}
			echo '</script>';
		}
	}

	/**
	 *
	 * Append appropriate query parameter to embed URLs to enable advanced tracking.
	 *
	 * @param string $return Embed code created by WP.
	 * @param object $data Response from oEmbed provider.
	 * @param string $url URL sent to oEmbed provider.
	 * @return string Embed code with query parameter appended.
	 */
	function enable_apis( $return, $data, $url ) {
		$settings = wpsf_get_settings( $this->settings_page );

		switch ( $data->provider_name ) {
			case 'YouTube':
				if ( '1' === $settings['settingsgas_youtube_track'] ) {
					$return = str_replace( '?feature=oembed', '?feature=oembed&amp;enablejsapi=1', $return );
				}
		}
		return $return;
	}
}
new GAS();
?>
