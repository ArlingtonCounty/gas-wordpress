## Google Analytic on Steroids WordPress Plugin ##

[Google Analytics on Steroids](https://github.com/CardinalPath/gas) (GAS) is a
powerful wrapper library for Google Analytics that enables enhanced tracking, including:

* Outbound links
* Downloads
* Forms
* mailto:
* YouTube, Vimeo, and HTML5 video events

This plugin makes it easy to add GAS to your WordPress site. All logged-out visitors and logged-in users without
the [edit_posts](https://codex.wordpress.org/Roles_and_Capabilities#edit_posts)
capability (i.e. [Subscriber](https://codex.wordpress.org/Roles_and_Capabilities#Subscriber)
) are tracked.

Written by the [Arlington County](https://www.arlingtonva.us) Department of Technology Services. Thanks to:

* [Cardinal Path](http://www.cardinalpath.com/) for [Google Analytics on Steroids](https://github.com/CardinalPath/gas)
* [Gilbert Pellegrom](http://gilbert.pellegrom.me/) for the [WordPress Settings Framework](https://github.com/gilbitron/WordPress-Settings-Framework)
* [Phillihp Harmon](http://phillihp.com/) for code contributions

## Installation ##

Tested with WordPress 3.5-4.9.6

1. Upload `gas-wordpress` to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress
1. Go to Settings > Google Analytics on Steroids and fill in your GA property ID(s) and domains(s). All other fields are optional.
