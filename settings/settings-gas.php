<?php

/*
Copyright 2015 County Board of Arlington County (email: webmaster@arlingtonva.us)

This file is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

global $wpsf_settings;

// General Settings section.
$wpsf_settings[] = array(
	'section_id'          => 'ga',
	'section_title'       => 'Google Analytics Properties',
	'section_description' => 'At least one ID and domain are required.',
	'section_order'       => 5,
	'fields'              => array(
		array(
			'id'          => 'property-ids',
			'title'       => 'Property ID(s)',
			'desc'        => 'Comma-separated list of <abbr title="Google Analytics">GA</abbr> property IDs',
			'type'        => 'text',
			'placeholder' => 'UA-00000000-X,UA-00000001-X',
			'std'         => '',
		),
		array(
			'id'          => 'domains',
			'title'       => 'Domain(s)',
			'desc'        => 'Comma-separated list of domains. Use leading periods to allow tracking of second-level subdomains',
			'type'        => 'text',
			'placeholder' => '.example.com,.example.org',
			'std'         => '',
		),
	),
);

$wpsf_settings[] = array(
	'section_id'          => 'dnt',
	'section_title'       => 'Do Not Track',
	'section_description' => 'All logged-out visitors and logged-in users without the <a href="https://codex.wordpress.org/Roles_and_Capabilities#edit_posts">edit_posts</a> capability (i.e. <a href="https://codex.wordpress.org/Roles_and_Capabilities#Subscriber">Subscriber</a>) are tracked by default',
	'section_order'       => 10,
	'fields'              => array(
		array(
			'id'    => 'header',
			'title' => 'Header',
			'desc'  => 'Honor <a href="http://donottrack.us/">Do Not Track HTTP header</a>',
			'type'  => 'checkbox',
			'std'   => 0,
		),
	),
);

$wpsf_settings[] = array(
	'section_id'    => 'page',
	'section_title' => 'Page Tracking',
	'section_order' => 15,
	'fields'        => array(
		array(
			'id'    => 'trackpageviews',
			'title' => 'Pageviews',
			'desc'  => 'Track pageviews',
			'type'  => 'checkbox',
			'std'   => 1,
		),
		array(
			'id'    => 'trackoutboundlinks',
			'title' => 'Outbound Links',
			'desc'  => 'This function will look for any outbound links on the current page and will trigger an event when the link is clicked (mousedown)',
			'type'  => 'checkbox',
			'std'   => 1,
		),
		array(
			'id'    => 'trackmailto',
			'title' => 'mailto:',
			'desc'  => 'Tracks clicks on links with <code>href="mailto:"</code>',
			'type'  => 'checkbox',
			'std'   => 1,
		),
		array(
			'id'    => 'trackforms',
			'title' => 'Forms',
			'desc'  => 'Trigger events every time a user submits a form or changes a form field',
			'type'  => 'checkbox',
			'std'   => 0,
		),
		array(
			'id'    => 'trackmaxscroll',
			'title' => 'Max Scroll',
			'desc'  => 'Fire events with the Max-Scroll percentage value for every page the user views',
			'type'  => 'checkbox',
			'std'   => 0,
		),
		array(
			'id'    => 'trackprint',
			'title' => 'Print',
			'desc'  => 'Trigger events every time a user prints',
			'type'  => 'checkbox',
			'std'   => 1,
		),
	)
);

$wpsf_settings[] = array(
	'section_id'    => 'download',
	'section_title' => 'Download Tracking',
	'section_order' => 20,
	'fields'        => array(
		array(
			'id'    => 'track',
			'title' => 'Downloads',
			'desc'  => 'When enabled, GAS will track the following extensions: xls, xlsx, doc, docx, ppt, pptx, pdf, txt, zip, rar, 7z, exe, wma, mov, avi, wmv, mp3, csv, tsv',
			'type'  => 'checkbox',
			'std'   => 1,
		),
		array(
			'id'    => 'extensions',
			'title' => 'Additional Extensions',
			'desc'  => 'Comma-separated list of additional extensions to track',
			'type'  => 'text',
			'std'   => '',
		),
		array(
			'id'    => 'category',
			'title' => 'Event Category',
			'desc'  => '',
			'type'  => 'text',
			'std'   => 'Download',
		),
	)
);

$wpsf_settings[] = array(
	'section_id'    => 'youtube',
	'section_title' => 'YouTube Tracking',
	'section_order' => 25,
	'fields'        => array(
		array(
			'id'    => 'track',
			'title' => 'YouTube',
			'desc'  => 'Track YouTube video events (play, pause, finish, error). <code>enablejsapi=1</code> will be appended to the iframe src of oEmbeds created by WordPress in new or updated posts. <a href="https://github.com/cardinalpath/gas#_gastrackyoutube">More information</a>.',
			'type'  => 'checkbox',
			'std'   => 1,
		),
		array(
			'id'    => 'forcereload',
			'title' => 'Force Reload',
			'desc'  => 'If <code>enablejsapi=1</code> not in iframe src, add and force reload',
			'type'  => 'checkbox',
			'std'   => 1,
		),
		array(
			'id'     => 'percentages',
			'title'  => 'Percentages',
			'desc'   => 'Percentages to track in addition to the default events. Include 1% to distinguish starts from restarts.',
			'type'   => 'text',
			'std'    => '1,20,40,60,80',
		),
		array(
			'id'    => 'category',
			'title' => 'Event Category',
			'desc'  => '',
			'type'  => 'text',
			'std'   => 'YouTube Video',
		),
	)
);

$wpsf_settings[] = array(
	'section_id'    => 'vimeo',
	'section_title' => 'Vimeo Tracking',
	'section_order' => 30,
	'fields'        => array(
		array(
			'id'    => 'track',
			'title' => 'Vimeo',
			'desc'  => 'Track Vimeo video events (play, pause, finish). Include <code>api=1</code> in iframe src. <a href="https://github.com/cardinalpath/gas#_gastrackvimeo">More information</a>.',
			'type'  => 'checkbox',
			'std'   => 1,
		),
		array(
			'id'    => 'forcereload',
			'title' => 'Force Reload',
			'desc'  => 'If <code>api=1</code> not in iframe src, add and force reload',
			'type'  => 'checkbox',
			'std'   => 1,
		),
		array(
			'id'    => 'category',
			'title' => 'Event Category',
			'desc'  => '',
			'type'  => 'text',
			'std'   => 'Vimeo Video',
		),
	)
);

$wpsf_settings[] = array(
	'section_id'    => 'html5',
	'section_title' => 'HTML5 Media Tracking',
	'section_order' => 35,
	'fields'        => array(
		array(
			'id'    => 'audio',
			'title' => 'Audio',
			'desc'  => 'Track HTML5 Audio events (play, pause, ended)',
			'type'  => 'checkbox',
			'std'   => 1,
		),
		array(
			'id'    => 'video',
			'title' => 'Video',
			'desc'  => 'Track HTML5 Video events (play, pause, ended)',
			'type'  => 'checkbox',
			'std'   => 1,
		),
	)
);
